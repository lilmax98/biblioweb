<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Livre;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExemplaireRepository")
 * @ORM\Table(name="exemplaire")
 */
class Exemplaire
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer",name="numero")
     */
    private $numero;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\Livre")
     * @ORM\JoinColumn(name="idlivre", referencedColumnName="idlivre")
     */
    private $livre;

    /**
     * @ORM\Column(type="date")
     */
    private $dateretour;

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function getDateretour(): ?\DateTimeInterface
    {
        return $this->dateretour;
    }

    public function setDateretour(\DateTimeInterface $dateretour): self
    {
        $this->dateretour = $dateretour;

        return $this;
    }

    public function getLivre(): ?Livre
    {
        return $this->livre;
    }

    public function setLivre(?Livre $livre): self
    {
        $this->livre = $livre;

        return $this;
    }


}