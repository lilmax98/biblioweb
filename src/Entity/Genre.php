<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
/**

 * @ORM\Entity(repositoryClass="App\Repository\GenreRepository")

 * @ORM\Table(name="genre")

 */
class Genre
{
    /**

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="SEQUENCE")

     * @ORM\SequenceGenerator(sequenceName="genre_idgenre_seq")

     * @ORM\Column(type="integer",name="idgenre")

     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $libelle;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Livre",mappedBy = "lesGenres")
     * @ORM\JoinTable(name="appartenir",
     *     joinColumns={@ORM\JoinColumn(name="idlivre", referencedColumnName="idlivre")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="idgenre", referencedColumnName="idgenre")}
     *     )
     */
    private $lesLivres;


    /**
     * Genre constructor.
     */
    public function __construct()
    {
        $this->lesLivres = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|Livre[]
     */
    public function getLesLivres(): Collection
    {
        return $this->lesLivres;
    }

    public function addLesLivre(Livre $lesLivre): self
    {
        if (!$this->lesLivres->contains($lesLivre)) {
            $this->lesLivres[] = $lesLivre;
        }

        return $this;
    }

    public function removeLesLivre(Livre $lesLivre): self
    {
        $this->lesLivres->removeElement($lesLivre);

        return $this;
    }

    public function __toString()
    {
        return $this->getLibelle();
    }

}