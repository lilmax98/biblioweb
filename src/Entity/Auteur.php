<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**

 * @ORM\Entity(repositoryClass="App\Repository\AuteurRepository")

 * @ORM\Table(name="auteur")

 */

class Auteur

{

    /**

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="SEQUENCE")

     * @ORM\SequenceGenerator(sequenceName="auteur_idauteur_seq")

     * @ORM\Column(type="integer",name="idauteur")

     */

    private $id;


    /**

     * @ORM\Column(type="string")

     */

    private $prenom;

    /**

     * @ORM\Column(type="string")

     */

    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Livre", mappedBy="auteur")
     */
    private $lesLivres;

    /**
     * Auteur constructor.
     */
    public function __construct()
    {
        $this->lesLivres = new ArrayCollection();
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Auteur", inversedBy="lesLivres")
     * @ORM\JoinColumn(name="idauteur", referencedColumnName="idauteur")
     */
    private $auteur;

    public function __toString()
    {
        return $this->getNom(). ' '.$this->getPrenom();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Livre[]
     */
    public function getLesLivres(): Collection
    {
        return $this->lesLivres;
    }

    public function addLesLivre(Livre $lesLivre): self
    {
        if (!$this->lesLivres->contains($lesLivre)) {
            $this->lesLivres[] = $lesLivre;
            $lesLivre->setAuteur($this);
        }

        return $this;
    }

    public function removeLesLivre(Livre $lesLivre): self
    {
        if ($this->lesLivres->removeElement($lesLivre)) {
            // set the owning side to null (unless already changed)
            if ($lesLivre->getAuteur() === $this) {
                $lesLivre->setAuteur(null);
            }
        }

        return $this;
    }

    public function getAuteur(): ?self
    {
        return $this->auteur;
    }

    public function setAuteur(?self $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }
}