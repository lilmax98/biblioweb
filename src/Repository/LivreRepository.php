<?php

namespace App\Repository;

use App\Entity\Genre;
use App\Entity\Livre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Livre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Livre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Livre[]    findAll()
 * @method Livre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LivreRepository extends ServiceEntityRepository
{


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Livre::class);
    }

    // /**
    //  * @return Livre[] Returns an array of Livre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Livre
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    */

    public function findByAuteur($auteur)
    {
        $qb = $this->createQueryBuilder('l')
            ->where('l.auteur = :auteur')
            ->setParameter('auteur', $auteur)
            ->orderBy('l.titre');

        $query = $qb->getQuery();

        return $query->execute();

    }

    public function findAllByGenre(Genre $genre)
    {
        $qb = $this->createQueryBuilder('l')
            ->leftJoin('l.lesGenres', 'g')
            ->where('g = :genre')
            ->setParameter('genre', $genre)
            ->orderBy('l.titre');
        $query = $qb->getQuery();

        return $query->execute();
    }

    public function findByTitre($titre)
    {
        $qb = $this->createQueryBuilder('l')
            ->where('l.titre LIKE :titre')
            ->setParameter('titre', '%' . $titre . '%')
            ->orderBy('l.titre');

        $query = $qb->getQuery();

        return $query->execute();
    }
}
