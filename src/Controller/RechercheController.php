<?php


namespace App\Controller;

use App\Entity\Livre;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\AbstractType;
use App\Entity\Auteur;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RechercheController extends AbstractController
{

    public function rechercheParAuteur(Request $request){
        $formRechAut = $this->createFormBuilder()
            ->add('auteur',EntityType::class,[
                'class'=> Auteur::class])
            ->add('rechercher', SubmitType::class)
            ->getForm();
        $formRechAut->handleRequest($request);
        if ($formRechAut->isSubmitted()){
            $auteur=$formRechAut->getData()['auteur'];
            return $this->redirectToRoute('auteurLivres',['id' => ($auteur->getId())]);
        }
        return $this->render('recherche/rechercheParAuteur.html.twig',['formRechAut' => $formRechAut->createView()]);
    }
    public function rechercheParTitre(Request $request)
    {
        $formRechTitr = $this->createFormBuilder()
            ->add('titre', TextType::class)
            ->add('rechercher', SubmitType::class)
            ->getForm();
        $formRechTitr->handleRequest($request);
        if ($formRechTitr->isSubmitted()) {
            $recherche=$formRechTitr->getData();
            $titre = $formRechTitr->getData()['titre'];
            $livres = $this->getDoctrine()->getRepository(Livre::class)->findByTitre($titre);
            return $this->render('livres.html.twig', ['titre' => "Résultat pour le titre contenant " . $recherche['titre'], 'livres' => $livres]);
        }
        return $this->render('recherche/rechercheParTitre.html.twig', ['formRechTitr' => $formRechTitr->createView()]);
    }


}