<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index()
    {
        return $this->render('index.html.twig',['titre'=>"Page d'accueil"]);
    }

    public function connexion()
    {
        return $this->render('connexion.html.twig',['titre'=>"Connexion"]);
    }

    public function recherche()
    {
        return $this->render('recherche.html.twig',['titre'=>"Rechercher un livre"]);
    }

    public function informationspratiques(){
        return $this->render('informationspratiques.html.twig',['titre'=>"Informations pratiques"]);
    }
}
