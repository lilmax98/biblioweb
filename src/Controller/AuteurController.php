<?php

namespace App\Controller;

use App\Entity\Auteur;
use App\Entity\Livre;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuteurController extends AbstractController
{
    /**
     * @Route("/auteurs", name="auteurs")
     */
    public function auteurs()
    {
        $auteurs = $this->getDoctrine()->getRepository(Auteur::class)->findAll();
        return $this->render('auteur/index.html.twig', ['titre' => "Liste des auteurs", 'auteurs' => $auteurs]);
    }
    public function getLivreAuteurbyID($id){
        $auteur = $this->getDoctrine()->getRepository(Auteur::class)->find($id);
        return $this->render('auteur/unauteur.html.twig',['titre' => "Liste des livres de l'auteur", 'auteur' => $auteur]);
    }

    /**
     * @Route("/auteur/{id}/livre", name="auteurLivres")
     */
    public function auteurLivres($id)
    {
        $auteur = $this->getDoctrine()->getRepository(Auteur::class)->find($id);
        $livres = $this->getDoctrine()->getRepository(Livre::class)->findByAuteur($auteur);

        return $this->render('livres.html.twig', [
            'auteur'=>$auteur,
            'livres' => $livres,
            'titre' => "Liste des livres de l'auteur"
        ]);
    }
}