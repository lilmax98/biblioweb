<?php

namespace App\Controller;

use App\Entity\Auteur;
use App\Entity\Exemplaire;
use App\Entity\Genre;
use App\Entity\Livre;
use App\FormType\LivreType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class LivreController extends AbstractController
{
    public function livres(){
        $livres = $this->getDoctrine()->getRepository(Livre::class)->findAll();
        return $this->render('livres.html.twig',['titre'=>"Liste des livres",'livres'=>$livres]);
    }

    public function unlivre($id){
        $livre = $this->getDoctrine()->getRepository(Livre::class)->find($id);
        $exemplaires = $this->getDoctrine()->getRepository(Exemplaire::class)->findByLivre($livre);
        return $this->render('unlivre.html.twig',[
            'livre'=>$livre,
            'exemplaires' => $exemplaires]);
    }

    public function genresLivre($id)
    {
        $livre = $this->getDoctrine()->getRepository(Livre::class)->find($id);
        return $this->render('genresLivre.html.twig', [
            'livre' => $livre,
            'titre' => "Liste des genres d'un livre"
        ]);
    }

    public function exemplaires()
    {
        $exemplaires = $this->getDoctrine()->getRepository(Exemplaire::class)->findAll();
        return $this->render('exemplaire/listeExemplaires.html.twig', ['titre' => "Liste des exemplaires", 'exemplaires' => $exemplaires]);
    }

    public function newLivre(Request $request){
        $formLivre = $this->createFormBuilder()
            ->add('titre',TextType::class)
            ->add('resume',TextType::class)
            ->add('isbn',TextType::class)
            ->add('auteur',EntityType::class,[
                'class'=> Auteur::class])
            ->add('lesGenres', EntityType::class,['class'=>Genre::class, 'multiple'=>true])
            ->add('enregistrer',SubmitType::class)
            ->getForm();
        $livre = new Livre();
        $formLivre = $this->createForm(LivreType::class,$livre);
        $formLivre->handleRequest($request); // $request est un objet transmis en paramètre de la fonction

        if ($formLivre->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($livre); //stocké en mémoire dans la collection de livres
            $entityManager->flush(); // synchronisation avec la BDD -> production d'un ordre SQL de type INSERT
            return $this->redirectToRoute('livres', ['titre'=>'Ajouter un livre']);

        }

        return $this->render('formulaireLivre.html.twig', ['titre'=>"Liste des livres", 'formLivre' => $formLivre->createView()]);
    }
}