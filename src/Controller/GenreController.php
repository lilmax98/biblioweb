<?php


namespace App\Controller;
use App\Entity\Auteur;
use App\Entity\Genre;
use App\Entity\Livre;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GenreController extends AbstractController
{

    public function getLivreByIdGenre($id)
    {
        $genre = $this->getDoctrine()->getRepository(Genre::class)->find($id);
        $livres = $this->getDoctrine()->getRepository(Livre::class)->findAllByGenre($genre);
        return $this->render('livresGenre.html.twig', [
            'genre'=>$genre,
            'livres' => $livres,
            'titre' => "Liste des livres d'un genre"]);
    }
}

